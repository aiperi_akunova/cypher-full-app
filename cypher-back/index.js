const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const cors = require('cors');

const app =express();
app.use(express.json());
app.use(cors());
const port = 8000;


// app.get('/encode/:text', (req,res)=>{
//     const answer = Vigenere.Cipher(password).crypt(req.params.text);
//     res.send(answer);
// });
//
// app.get('/decode/:text', (req,res)=>{
//     const answer = Vigenere.Decipher(password).crypt(req.params.text);
//     res.send(answer);
// });


app.post('/encode', (req,res)=>{
    const answer = Vigenere.Cipher(req.body.password).crypt(req.body.decoded);
    res.send({encoded: answer});
});

app.post('/decode', (req,res)=>{
    const answer = Vigenere.Decipher(req.body.password).crypt(req.body.encoded);
    res.send({decoded: answer});
});

app.listen(port, ()=>{
    console.log('We are on port '+port)
});