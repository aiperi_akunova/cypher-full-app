import {
    POST_DECODE_FAILURE,
    POST_DECODE_REQUEST,
    POST_DECODE_SUCCESS,
    POST_ENCODE_FAILURE,
    POST_ENCODE_REQUEST,
    POST_ENCODE_SUCCESS
} from './actions'

const initialState = {
    encoded: null,
    decoded: null,
    error: null,
    loading: false,
}

const reducer = (state= initialState, action)=>{
    switch (action.type){
        case POST_ENCODE_REQUEST:
            return {...state, error: null, loading: true};
        case POST_ENCODE_SUCCESS:
            return {...state, loading: false, encoded: action.payload.encoded};
        case POST_ENCODE_FAILURE:
            return {...state, loading: false, error: action.payload};
        case POST_DECODE_REQUEST:
            return {...state, error: null, loading: true};
        case POST_DECODE_SUCCESS:
            return {...state, loading: false, decoded: action.payload.decoded};
        case POST_DECODE_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}

export default reducer;