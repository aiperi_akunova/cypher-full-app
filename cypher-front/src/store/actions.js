import axios from "axios";

export const POST_ENCODE_REQUEST = 'POST_ENCODE_REQUEST';
export const POST_ENCODE_SUCCESS = 'POST_ENCODE_SUCCESS';
export const POST_ENCODE_FAILURE = 'POST_ENCODE_FAILURE';

export const POST_DECODE_REQUEST = 'POST_DECODE_REQUEST';
export const POST_DECODE_SUCCESS = 'POST_DECODE_SUCCESS';
export const POST_DECODE_FAILURE = 'POST_DECODE_FAILURE';

export const postEncodeRequest = () => ({type: POST_ENCODE_REQUEST});
export const postEncodeSuccess = response => ({type: POST_ENCODE_SUCCESS, payload: response});
export const postEncodeFailure = (error) => ({type: POST_ENCODE_FAILURE, payload: error});

export const postDecodeRequest = () => ({type: POST_DECODE_REQUEST});
export const postDecodeSuccess = response => ({type: POST_DECODE_SUCCESS, payload: response});
export const postDecodeFailure = (error) => ({type: POST_DECODE_FAILURE, payload: error});

export const postEncode =message => {
    return async dispatch => {
        try {
            // dispatch(postEncodeRequest());
            const response = await axios.post('http://127.0.0.1:8000/encode', message);
            dispatch(postEncodeSuccess(response.data))
        } catch (error) {
            dispatch(postEncodeFailure(error))
        }
    }
}

export const postDecode = message => {
    return async dispatch => {
        try {
            // dispatch(postDecodeRequest());
            const response = await axios.post('http://127.0.0.1:8000/decode', message);
            dispatch(postDecodeSuccess(response.data))
        } catch (error) {
            dispatch(postDecodeFailure(error))
        }
    }
}