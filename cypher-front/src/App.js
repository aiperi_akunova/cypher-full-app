import './App.css';
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {postEncode} from "./store/actions";
import {postDecode} from "./store/actions";

const App = () => {
    const dispatch = useDispatch();
    const encodedMessage = useSelector(state => state.encoded);
    const decodedMessage = useSelector(state => state.decoded);

    const [message, setMessage] = useState({
        password: '',
        encoded: '',
        decoded: '',
    })

    const onInputChange = e => {
        const {name, value} = e.target;

        setMessage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const encode =()=>{

        if(message.password === '' || message.password === null || message.decoded === '') {
           alert('Enter password or message') ;
        } else {
            dispatch(postEncode({password: message.password, decoded: message.decoded}))
        }
    }
    const decode =()=>{
        if(message.password === '' || message.password === null || message.encoded === ''){
            alert('Enter password or message') ;
        } else {
            dispatch(postDecode({password: message.password, encoded: message.encoded}))
        }
    }

  return (
      <div className="App">
       <h1>Cypher app!</h1>
          <div className='box'>
              <label htmlFor='decoded'>
                  Decoded message:
              </label>
              <div>
                  {decodedMessage && <p>{decodedMessage}</p>}
              </div>
              <input
                  className='cipher-input'
                  type="text"
                  name='decoded'
                  onChange={onInputChange}
                  value={message.decoded}

              />



              <div className='box2'>
                  <div>
                      <label htmlFor='password'>
                          Password:
                      </label>
                      <input
                          className='password'
                          type="text"
                          name='password'
                          onChange={onInputChange}
                          value={message.password}
                      />
                  </div>
                  <div>
                      <button onClick={decode}> &uarr;</button>
                      <button onClick={encode}> &darr;</button>
                  </div>
              </div>

              <label htmlFor='encoded'>
                  Encoded message:
              </label>
              <div>
                  {encodedMessage && <p>{encodedMessage}</p>}
              </div>
              <input
                  className='cipher-input'
                  type="text"
                  name='encoded'
                  onChange={onInputChange}
                  value={message.encoded}
              />
          </div>
      </div>
  );
};

export default App;
